#!/usr/bin/env bash

#install tools
apt-get update

apt-get install -y gawk wget git-core diffstat unzip texinfo gcc-multilib \
  build-essential chrpath socat libsdl1.2-dev xterm vim mtd-utils

#install ssh config
cp /vagrant/.ssh/* .ssh
chmod 644 .ssh/known_hosts
ssh-keyscan -t rsa bitbucket.org >> ~/.ssh/known_hosts

#install compilers for arm
mkdir -p .local/compilers
cd .local/compilers

cp /vagrant/arm-none-gcc.tar.bz2 .
cp /vagrant/gcc-armhf.tar.bz2 .

tar -jxf arm-none-gcc.tar.bz2
tar -jxf gcc-armhf.tar.bz2

rm -Rf *.tar.bz2

cd /home/vagrant

echo "" >> .bashrc
echo "export CCSYSROOTS=\$HOME/.local/compilers/gcc-armhf" >> .bashrc
echo "export PATH=\$PATH:\$HOME/.local/compilers/gcc-armhf/i686-linux/usr/bin/arm-poky-linux-gnueabi:\$HOME/.local/compilers/arm/bin" >> .bashrc

#install sample makefile
cp -R /vagrant/sample_code/ .

chown -R vagrant:vagrant /home/vagrant



